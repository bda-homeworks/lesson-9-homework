import java.util.Scanner;

public class Calculator {
    public static int toplama(int num1, int num2) {
        int num3;
        num3 = num1 + num2;
        return num3;
    }
    public static int cixma(int num1, int num2) {
        int num3;
        num3 = num1 - num2;
        return num3;
    }
    public static int vurma(int num1, int num2) {
        int num3;
        num3 = num1 * num2;
        return num3;
    }
    public static int bolme(int num1, int num2) {
        int num3;
        num3 = num1 / num2;
        return num3;
    }

    public static void main(String[] args) {
        System.out.println("1: +");
        System.out.println("2: -");
        System.out.println("3: *");
        System.out.println("4: /");
        Scanner input = new Scanner(System.in);
        int operator = input.nextInt();
        switch (operator) {
            case 1:
                System.out.println("Toplamaq istediyiniz birinci ve ikinci reqemi qeyd edin:");
                System.out.println(toplama(input.nextInt(), input.nextInt()));
                break;
            case 2:
                System.out.println("Cixmaq istediyiniz birinci ve ikinci reqemi qeyd edin:");
                System.out.println(cixma(input.nextInt(), input.nextInt()));
                break;
            case 3:
                System.out.println("Vurmaq istediyiniz birinci ve ikinci reqemi qeyd edin:");
                System.out.println(vurma(input.nextInt(), input.nextInt()));
                break;
            case 4:
                System.out.println("Vurmaq istediyiniz birinci ve ikinci reqemi qeyd edin:");
                System.out.println(bolme(input.nextInt(), input.nextInt()));
                break;
            default:
                System.out.println("Bele bir secim yoxdur!");
        }
    }
}