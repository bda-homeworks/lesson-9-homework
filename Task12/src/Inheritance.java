import java.util.Scanner;

public class Inheritance {
    public static void main(String[] args) {
        System.out.println("Enter the brand of the car:");
        System.out.println("Then after entering brand enter how many wheels in your car:");
        Scanner input = new Scanner(System.in);
        Car carA = new Car(input.next(), input.nextInt());
        carA.honk();
        carA.drive();
    }
}
