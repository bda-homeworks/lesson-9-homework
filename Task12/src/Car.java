public class Car extends Vehicle{
    private int numWheels;

    public Car(String brand, int numWheels) {
        super(brand);
        this.numWheels = numWheels;
    }

    public void drive () {
        System.out.println("Driving car with " + numWheels + " wheels");
    }
}
