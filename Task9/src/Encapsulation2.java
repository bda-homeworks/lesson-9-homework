import java.util.Scanner;

public class Encapsulation2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Encapsulation1 enc1 = new Encapsulation1();
        System.out.println("Ad ve Yas qeyd edin Encapsulation1 class-inin private ad ve yas fieldi ucun:");
        enc1.setName(input.next());
        enc1.setAge(input.nextInt());
        System.out.println("Private fieldlerden alinan melumatlar");
        System.out.print(enc1.getName());
        System.out.print(" ");
        System.out.print(enc1.getAge());
    }
}
