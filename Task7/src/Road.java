public class Road {
    public static void main(String[] args) {
        Car sedan = new Sedan();
        Car truck = new Truck();

        sedan.accelerate();
        sedan.brake();
        sedan.engineOff();
        sedan.engineOn();
        sedan.park();

        truck.accelerate();
        truck.brake();
        truck.engineOff();
        truck.engineOn();
        truck.park();
    }
}
