abstract class Car {
    public abstract void engineOn ();
    public abstract void engineOff ();
    public abstract void accelerate ();
    public abstract void brake ();
    public abstract void park ();

}
