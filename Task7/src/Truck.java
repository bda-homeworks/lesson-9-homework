public class Truck extends Car{
    @Override
    public void engineOn() {
        System.out.println("Turning the engine on using key.");
    }

    @Override
    public void engineOff() {
        System.out.println("Turning the engine off by twisting the key in the left side of steering wheel.");
    }

    @Override
    public void accelerate() {
        System.out.println("Accelerating to top speed of 180km/h and it depends on what weight the truck carries.");
    }

    @Override
    public void brake() {
        System.out.println("Brake system is equipped with ceramic so to avoid crash situations.");
    }

    @Override
    public void park() {
        System.out.println("Doesn't come with automatic system only manual.");
    }
}
