public class Sedan extends Car{

    @Override
    public void engineOn() {
        System.out.println("Turning the engine using remote control or nfc.");
    }

    @Override
    public void engineOff() {
        System.out.println("Turning the engine off using button in right side of steering wheel.");
    }

    @Override
    public void accelerate() {
        System.out.println("Accelerating to top speed of 280km/h.");
    }

    @Override
    public void brake() {
        System.out.println("Braking power is 2800nm.");
    }

    @Override
    public void park() {
        System.out.println("Automatic parking system with 3d vision cameras.");
    }
}
