public class Supermarket extends Retail{

    private double pricePerUnit;
    public Supermarket (double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @Override
    double totalPriceGoods(int amount) {
        return pricePerUnit * amount;
    }
}
