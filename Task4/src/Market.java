public class Market extends Retail{
    private double pricePerUnit;

    public Market (double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }


    @Override
    double totalPriceGoods(int amount) {
        return pricePerUnit * amount;
    }
}
