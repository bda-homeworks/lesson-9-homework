import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the amount of goods for Supermarket");
        Supermarket supermarket = new Supermarket(input.nextDouble());
        System.out.println("Enter the price of unit");
        double supermarketTotalPrice = supermarket.totalPriceGoods(input.nextInt());

        System.out.println("Enter the amount of goods for Market");
        Market market = new Market(input.nextDouble());
        System.out.println("Enter the price of unit");
        double marketTotalPrice = market.totalPriceGoods(input.nextInt());

        System.out.println("Supermarket total price of goods: " + supermarketTotalPrice);
        System.out.println("Market total price of goods: " + marketTotalPrice);
    }
}
