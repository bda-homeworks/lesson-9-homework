public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        MyInterface myInterface = new MyInterface() {
            @Override
            public void doSomething() {
                System.out.println("Salam dostlar!");
            }
        };

        myInterface.doSomething();
    }
}
interface MyInterface {
    void doSomething ();
}

//Java compiler ise dusende arxa planda ozel bir ad acir class
//yada interface ucun hansiki ya extend olunur yada implement
//ve override olunacaq methodlar anonymous classin icinde yazilir
//buda bize hem elan elemeyi hemde instantiate etmeyi icaze verir