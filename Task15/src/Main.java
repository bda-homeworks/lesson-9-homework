import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Sira ile - Ad, Soyad, Vezife, Bolme ve Maas-inizi qeyd edin:");
        Employee employee = new Employee(input.next(), input.next(), input.next(), input.next(), input.nextInt());
        System.out.println();
        int choice;
        do {
            System.out.println("\n=== Employee Information Menu ===");
            System.out.println("1. Print all information");
            System.out.println("2. Print name");
            System.out.println("3. Print surname");
            System.out.println("4. Print position");
            System.out.println("5. Print department");
            System.out.println("6. Print salary");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            choice = input.nextInt();

            switch (choice) {
                case 1:
                    employee.printInfo();
                    break;
                case 2:
                    System.out.println("Name: " + employee.getName());
                    break;
                case 3:
                    System.out.println("Surname: " + employee.getSurname());
                    break;
                case 4:
                    System.out.println("Position: " + employee.getPosition());
                    break;
                case 5:
                    System.out.println("Department: " + employee.getDepartment());
                    break;
                case 6:
                    System.out.println("Salary: " + employee.getSalary());
                    break;
                case 0:
                    System.out.println("Leaving the system :( GoodBye!");
            }
        } while (choice != 0);

    }
}
