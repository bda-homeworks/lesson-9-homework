public class Math {
    public void toplama (int a, int b) {
        int result = a + b;
        System.out.println("Cavab: " + result);
    }
    public void toplama (int a, int b, int c) {
        int result = a + b + c;
        System.out.println("Cavab: " + result);
    }
    public void toplama (int a, int b, int c, int d) {
        int result = a + b + c + d;
        System.out.println("Cavab: " + result);
    }

    public void multiply(int a, int b) {
        int result = a * b;
        System.out.println("Multiplication Result: " + result);
    }

    public void multiply(int a, int b, int c) {
        int result = a * b * c;
        System.out.println("Multiplication Result: " + result);
    }

    public void multiply(int a, int b, int c, int d) {
        int result = a * b * c * d;
        System.out.println("Multiplication Result: " + result);
    }
}
