import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        User user = new User();
        Scanner input = new Scanner(System.in);
        user.setUsername(input.next());
        user.register(user);
        user.show();
    }
}