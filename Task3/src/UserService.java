abstract class UserService {
    public abstract void register(User user);
    public abstract void show();
}
