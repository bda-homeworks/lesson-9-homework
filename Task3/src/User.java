public class User extends UserService {
    private String username;

    @Override
    public void register(User user) {
        System.out.println("Registering user: " + user.getUsername());
    }

    @Override
    public void show() {
        System.out.println("Showing user details: " + getUsername());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
