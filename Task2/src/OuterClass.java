public class OuterClass {
    private static int outerData = 10;

    static class NestedClass {
        public void display() {
            System.out.println("Outer data: " + outerData);
        }
    }

    public static void main(String[] args) {
        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();
        nestedClass.display();
    }
}