public class OuterClass2 {
    int num;

    private class InnerClass {
        public void print() {
            System.out.println("This is an inner class");
        }
    }

    void show_info() {
        InnerClass inner = new InnerClass();
        inner.print();
    }

    public static class My_class {
        public static void main(String[] args) {
            OuterClass2 col_class = new OuterClass2();
            col_class.show_info();
        }
    }
}
