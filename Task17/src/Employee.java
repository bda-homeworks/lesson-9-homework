public class Employee extends Person{
    private String position;
    private String department;
    private double salary;

    public Employee(String name, String surname, String position, String department, double salary) {
        super(name, surname);
        this.position = position;
        this.department = department;
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public String getDepartment() {
        return department;
    }

    public double getSalary() {
        return salary;
    }

    public String getName() {
        return super.name;
    }

    public String getSurname() {
        return super.surname;
    }

    @Override
    public void printInfo() {
        System.out.println("Name: " + name);
        System.out.println("Surname: " + surname);
        System.out.println("Position: " + position);
        System.out.println("Department: " + department);
        System.out.println("Salary: " + salary);
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    public void setPosition(String position) {
        if (position != null && !position.isEmpty()) {
            this.position = position;
        } else {
            System.out.println("Position can't be empty");
        }
    }
}
