public class Animal {
    public static void main(String[] args) {
        AnimalAbstract cat = new Cat();
        AnimalAbstract dog = new Dog();

        cat.eat();
        cat.run();

        dog.eat();
        dog.run();
    }
}
